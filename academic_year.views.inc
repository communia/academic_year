<?php

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_views_data_alter().
 */
function academic_year_views_data_alter(array &$data) {
  $data['node_field_data']['academic_year'] = array(
    'title' => t('Academic year'),
    'field' => array(
      'title' => t('Academic year'),
      'help' => t('The academic year.'),
      'id' => 'academic_year',
    )
/*    'argument' => array(
      'handler' => 'academic_year_views_handler_argument_academic_year',
    )
*/
  );

}

