<?php
/**
 * @file
 * Definition of Drupal\academic_year\Plugin\views\field\AcademicYear
 */
namespace Drupal\academic_year\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("academic_year")
 */
class AcademicYear extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * Define the available options
   * @return array
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    //$options['node_type'] = array('default' => 'event');

    return $options;
  }


  /**
   * Provide the options form.
  */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $node = $values->_entity;
    if ($node->bundle() == 'event'){
      if ($node->field_data_inici->value){
          $dt = $node->field_data_inici->get(0)->get('date')->getValue();
          $academic_year = [
            '#type' => 'markup',
            '#prefix' => '<dl><dt class="inline">'.t('Academic year').'</dt><dd>',
            '#markup' => $dt ? academic_year_to_academic($dt): "",
            '#plain_text' => $dt ? academic_year_to_academic($dt): "",
            '#suffix' => '</dd></dl>',
          ];
          return Drupal\Core\Render\RendererInterface::render($academic_year);

      }
    }
  }
}
