<?php

namespace Drupal\academic_year\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 *  Provides a 'Academic year' summary Block
 *  @Block(
 *    id = "academic_block_summary",
 *    admin_label = @Translation("Academic Year Summary"),
 *  )
 */
class AcademicYearSummary extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var $entityTypeManager \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entity_type_manager;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    // Call parent construct method.
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // Store our dependencies.
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Use the $container to get a query factory object. This object let's us
    // create query objects.
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  private function getAcademicYears(){
    /*
    // Use the factory to create a query object for node entities.
    $query = $this->entityQuery->get('node');

    // Add a filter (published).
    $query->condition('status', 1);

    // Run the query.
    $nids = $query->execute();
     */
    $query = $this->entityTypeManager->getStorage('node')->getAggregateQuery();

    //Add afilter
    $query->condition('type', 'event')
      ->accessCheck(TRUE)
      ->condition('status', 1)
      ->groupBy('field_data_inici');
    $result = $query->execute();

    return $result;
  }

  private static function onlyYear($date){
    return substr($date['field_data_inici'], 0, 4);
  }

  private static function toAcademic($date){
    $text = (!empty($date))? $date."/".($date+1):t("All");
    $route_params = [ 
      "field_data_inici_value[min]" => (!empty($date))? $date."-09" : "",
      "field_data_inici_value[max]" => (!empty($date))? ($date+1)."-08": ""
    ];
    $url = \Drupal\Core\Url::fromRoute('view.activitats.page_1', $route_params );
    $link = [
      '#type' => 'link',
      '#url' => $url,
      '#title' => $text,
    ];
    return $link;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $years = $this->getAcademicYears();
    $only_years = array_map(array($this, 'onlyYear'), $years);
    array_push($only_years, $only_years[0] - 1);
    $unique_years = array_unique ($only_years);
    rsort($unique_years);
    $all_items = [""];
    $unique_years = $all_items + $unique_years;
    $academic_years = array_map(array($this, 'toAcademic'),  $unique_years);

    return array(
      '#theme' => 'item_list',
      '#attributes' => ['id' => 'academic_years_summary'],
      '#items' => $academic_years,
    );
  }

}
